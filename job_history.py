import sys
import os
import re
import pprint
import configparser
import argparse
#import gitlab
from pathlib import Path
import objects
def init_gitlab():
    '''Initiate the GitLab object with your gitlab configs

    :return: GitLab instance
    '''
    cfgfile = os.path.join(str(Path.home()), '.python-gitlab.cfg')
    if not os.path.isfile(cfgfile):
        raise FileNotFoundError
    cp = configparser.ConfigParser()
    mycfg = cp.read(cfgfile)
    try:
        toreturn = objects.Gitlab(cp['com']['url'], private_token=cp['com']['private_token'])
    except KeyError as e:
        raise e
    return toreturn



def parse_args(args):
    """
    Parse command line arguments

    :param args: command args

    :returns: argparse args
    """
    parser = argparse.ArgumentParser(description="Find failure reasons of jobs.")
    parser.add_argument('--pid', type=int, help='The ID of the project')
    parser.add_argument('--max_count', type=int, default=10, help='Max number of failures to load')
    parser.add_argument('--master_only', dest='master_only', action='store_true', help='Only look in master')
    pargs = parser.parse_args(args)
    return pargs

def get_failed_jobs_from_pipeline(pipeline_instance):
    print("Loading Failed Jobs from Pipeline {}".format(pipeline_instance.id))
    to_return = []
    for j in pipeline_instance.jobs.list():
        if j.status == 'failed':
            to_return.append(j)
    return to_return


def get_failed_pipelines_from_job(project, master_only=False, max_count=1):
    print("Loading Failed Pipeline History for {}".format(project.name))
    to_return = []
    ph = project.pipelines.list(as_list=False)
    for p in ph:
        if p.status == 'failed':
            if (not master_only) or (master_only and (p.ref == "master")):
                #print("Failed history length = {}".format(len(to_return)))
                to_return.append(p)
                if len(to_return) == max_count:
                    return to_return
    return p

def parse_job_logs(job, string):
    print("parse_job_logs(job, string)")
    the_logs = job.trace
    return(False)

def main(**kwargs):

    master_only = False

    pid = kwargs.get('pid')
    max_count = kwargs.get('max_count')
    master_only = kwargs.get('master_only')

    gl = init_gitlab()
    my_project = gl.projects.get(pid)

    failed_jobs = []
    failure_reasons = {}

    print("Load {} failed jobs from project {}".format(max_count, pid))

    for a_job in my_project.jobs.list(as_list = False):
        if a_job.status == 'failed' and ((not master_only) or (master_only and (a_job.ref == "master"))):

            aj = objects.Job(a_job)
            aj.parse_error()
            failed_jobs.append(aj.__dict__)

            # Populate failure reasons
            failure_reasons[aj.error] = (failure_reasons[aj.error] + 1) if aj.error in failure_reasons else 1

            print("{} jobs found".format(len(failed_jobs)))

        if len(failed_jobs) >= max_count:
            break


    # Now that we are done, print it
    pprint.pprint(failed_jobs)
    pprint.pprint(failure_reasons)

if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    sys.exit(main(**vars(args)))
