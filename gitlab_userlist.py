import sys
import os
import pprint
import configparser
import argparse
import gitlab
from pathlib import Path

def init_gitlab():
    '''Initiate the GitLab object with your gitlab configs

    :return: GitLab instance
    '''
    cfgfile = os.path.join(str(Path.home()), '.python-gitlab.cfg')
    if not os.path.isfile(cfgfile):
        raise FileNotFoundError
    cp = configparser.ConfigParser()
    mycfg = cp.read(cfgfile)
    try:
        toreturn = gitlab.Gitlab(cp['demo']['url'], private_token=cp['demo']['private_token'])
    except KeyError as e:
        raise e
    return toreturn



def parse_args(args):
    """
    Parse command line arguments

    :param args: command args

    :returns: argparse args
    """
    parser = argparse.ArgumentParser(description="List users in GitLab")
    group = parser.add_mutually_exclusive_group(required=True)
    parser.add_argument('--feature', dest='feature', action='store_true')
    group.add_argument('--pid', type=int, help='The ID of the project')
    group.add_argument('--gid', type=int, help='The ID of the group')
    parser.add_argument('--recursive', dest='recursive', action='store_true', default=False, help='Do you want to search recursive?')
    pargs = parser.parse_args(args)
    return pargs

def group_process(gl, gid, recursive):
    thegroup = gl.groups.get(gid)
    print("GROUP => {}".format(thegroup.name))
    if recursive:
        members = thegroup.members.all(all=recursive)
        projects = thegroup.projects.list()
        for p in projects:
            members += project_process(gl, p.id, recursive)
        groups = thegroup.subgroups.list()
        for g in groups:
            members += group_process(gl, g.id, recursive)
    else:
        members = thegroup.members.list()
    return members


def project_process(gl, pid, recursive):
    theproject = gl.projects.get(pid)
    print("PROJECT => {}".format(theproject.name))
    if recursive:
        members = theproject.members.all(all=recursive)
    else:
        members = theproject.members.list()
    return members
def main(**kwargs):

    gl = init_gitlab()

    pid = kwargs.get('pid')
    gid = kwargs.get('gid')
    recursive = kwargs.get('recursive')

    if gid:
        members = group_process(gl, gid, recursive)
    elif pid:
        members = project_process(gl, pid, recursive)

    print("-"*30)
    for m in members:
        pprint.pprint(m.__dict__)
    member_usernames = '","'.join(m.username for m in members)
    print('"' + member_usernames + '"')
    print("-"*30)

    member_names = '","'.join(m.name for m in members)
    print('"' + member_names + '"')
if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    sys.exit(main(**vars(args)))
