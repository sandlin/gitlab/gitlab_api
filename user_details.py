import sys
import os
import csv
import pprint
import configparser
import argparse
import datetime
from operator import attrgetter
from dateutil import parser
from dateutil.tz import tzutc
import gitlab
from pathlib import Path

class User(object):
    id = None
    name = None
    email = None
    last_activity_on = None
    last_sign_in_at = None

    def __init__(self, u):
        self.id = u.id
        self.is_admin = u.is_admin if hasattr(u, 'is_admin') else "n/a"
        self.name = u.name
        self.email= u.email if hasattr(u, 'email') else "n/a"
        self.state = u.state
        self.created_at = parser.parse(u.created_at) if hasattr(u, 'created_at') else datetime.datetime(1970, 1, 1, 0, 0, 0, tzinfo=tzutc())
        self.confirmed_at = parser.parse(u.confirmed_at) if hasattr(u, 'confirmed_at') else datetime.datetime(1970, 1, 1, 0, 0, 0, tzinfo=tzutc())
        self.last_activity_on = datetime.datetime.strptime(u.last_activity_on, '%Y-%m-%d') if hasattr(u, 'last_activity_on') else datetime.datetime(1970, 1, 1, 0, 0, 0, tzinfo=tzutc())
        self.last_sign_in_at = parser.parse(u.last_sign_in_at) if hasattr(u, 'last_sign_in_at') else datetime.datetime(1970, 1, 1, 0, 0, 0, tzinfo=tzutc())
        #super.__init__(super)

    def get_field_csv(self):
        return ['id', 'email', 'name', 'state', 'is_admin', 'created_at', 'confirmed_at', 'last_activity_on', 'last_sign_in_at']

    def to_csv(self):
        return [self.id, self.email, self.name, self.state, self.is_admin, self.created_at, self.confirmed_at, self.last_activity_on, self.last_sign_in_at]

def init_gitlab():
    '''Initiate the GitLab object with your gitlab configs

    :return: GitLab instance
    '''
    cfgfile = os.path.join(str(Path.home()), '.python-gitlab.cfg')
    if not os.path.isfile(cfgfile):
        raise FileNotFoundError
    cp = configparser.ConfigParser()
    mycfg = cp.read(cfgfile)
    try:
        toreturn = gitlab.Gitlab(cp['com']['url'], private_token=cp['com']['private_token'])
    except KeyError as e:
        raise e
    return toreturn

def main(**kwargs):

    gl = init_gitlab()
    user_objects = []
    #qlist = gl.users.list(all=True)
    qlist = gl.users.list()
    for u in qlist:
        user_objects.append(User(u))
    #################
    # sorted_list = sorted(user_objects, key=attrgetter('last_sign_in_at'))
    # If you want to print dicts:
    # for su in sorted_list:
    #     print(su.__dict__)
    #################
    # To generate CSV:
    with open('userreport.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow(user_objects[0].get_field_csv())
        for u in user_objects:
            writer.writerow(u.to_csv())
    print("userreport.csv created")
if __name__ == "__main__":
    sys.exit(main())
