import datetime
from dateutil import parser
from dateutil.tz import tzutc
import gitlab
import re
from gitlab.v4.objects import ProjectJob
import pprint

class Gitlab(gitlab.Gitlab):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)





class Job(object):
    ref = None
    status = None
    web_url = None
    error = None
    # This should be done via inheritence but I'm having issues & have a deadline.
    parent_obj = None

    def __init__(self, parent_job):
        self.ref = parent_job.ref
        self.status = parent_job.status
        self.web_url = parent_job.web_url
        self.parent_obj = parent_job

    def parse_error(self):
        error_sequence = re.compile(r"ERROR.*", re.MULTILINE)
        job_log = self.parent_obj.trace().decode("utf-8")
        self.error = error_sequence.search(job_log).group()
