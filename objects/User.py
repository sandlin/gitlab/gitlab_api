import datetime
from dateutil import parser
from dateutil.tz import tzutc

class User(object):
    id = None
    name = None
    email = None
    last_activity_on = None
    last_sign_in_at = None

    def __init__(self, u):
        self.id = u.id
        self.is_admin = u.is_admin
        self.name = u.name
        self.email= u.email
        self.state = u.state
        self.created_at = parser.parse(u.created_at) if u.created_at else datetime.datetime(1970, 1, 1, 0, 0, 0, tzinfo=tzutc())
        self.confirmed_at = parser.parse(u.confirmed_at) if u.confirmed_at else datetime.datetime(1970, 1, 1, 0, 0, 0, tzinfo=tzutc())
        self.last_activity_on = datetime.datetime.strptime(u.last_activity_on, '%Y-%m-%d') if u.last_activity_on else datetime.datetime(1970, 1, 1, 0, 0, 0, tzinfo=tzutc())
        self.last_sign_in_at = parser.parse(u.last_sign_in_at) if u.last_sign_in_at else datetime.datetime(1970, 1, 1, 0, 0, 0, tzinfo=tzutc())
        #super.__init__(super)

    def get_field_csv(self):
        return ['id', 'email', 'name', 'state', 'is_admin', 'created_at', 'confirmed_at', 'last_activity_on', 'last_sign_in_at']

    def to_csv(self):
        return [self.id, self.email, self.name, self.state, self.is_admin, self.created_at, self.confirmed_at, self.last_activity_on, self.last_sign_in_at]
