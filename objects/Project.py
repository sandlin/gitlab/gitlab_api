import datetime
from dateutil import parser
from dateutil.tz import tzutc
import gitlab
import pprint

class Project(object):
    archived = None
    created_at = None
    default_branch = None
    empty_repo = None
    jobs_enabled = None
    last_activity_at = None
    name = None
    name_with_namespace = None
    path_with_namespace = None
    permissions = None
    ssh_url_to_repo = None
    visibility = None
    star_count = None

    # def __init__(self, pid, gitlab_instance):
    #     proj = gitlab_instance.projects.get(pid)
    #     pprint.pprint(proj.__dict__)


    def get_failed_pipelines(self, master_only=False, max_count=1):
        print("Loading Failed Pipeline History for {}".format(self.name))
        to_return = []
        ph = self.pipelines.list(as_list=False)
        for p in ph:
            if p.status == 'failed':
                if (not master_only) or (master_only and (p.ref == "master")):
                    #print("Failed history length = {}".format(len(to_return)))
                    to_return.append(p)
                    if len(to_return) == max_count:
                        return to_return
        self.failed_pipelines = to_return
        return p
