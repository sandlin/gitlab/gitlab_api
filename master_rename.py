import sys
import os
import pprint
import json
import configparser
import argparse
import gitlab
from pathlib import Path

'''
HOW TO USE:
EX: 
python master_rename.py --pid 16769432 --newname groot
'''

class ProtectedBranch():

    push_access_levels = []
    merge_access_levls = []
    code_owner_approval_required = False

    def __init__(self, name):
        self.name = name
        super.__init__(self)


def init_gitlab():
    '''Initiate the GitLab object with your gitlab configs

    :return: GitLab instance
    '''
    cfgfile = os.path.join(str(Path.home()), '.python-gitlab.cfg')
    if not os.path.isfile(cfgfile):
        raise FileNotFoundError
    cp = configparser.ConfigParser()
    mycfg = cp.read(cfgfile)
    try:
        toreturn = gitlab.Gitlab(cp['com']['url'], private_token=cp['com']['private_token'])
    except KeyError as e:
        raise e
    return toreturn

def parse_args(args):
    """
    Parse command line arguments

    :param args: command args

    :returns: argparse args
    """
    parser = argparse.ArgumentParser(description="Replace master with newname")
    #group = parser.add_mutually_exclusive_group(required=True)
    parser.add_argument('--newname', dest='newname', required=True, help="The name of the branch to replace master")
    parser.add_argument('--pid', type=int, help='The ID of the project', required=True)
    pargs = parser.parse_args(args)
    return pargs

def div(title=None):
    if title:
        pre = "-="*10
        post = "=-"*10
        mid = "- {} -".format(title)
    else:
        pre = "-="*15
        post = "=-"*15
        mid = "-"
    print("{}{}{}".format(pre, mid, post))

def print_all_branches(project):
    branches = project.branches.list()
    div()
    for b in branches:
        pprint.pprint(b.__dict__)
    div()

def print_protected_branches(project):
    div("ProtectedBranches")
    p_branches = project.protectedbranches.list()
    for b in p_branches:
        pprint.pprint(b.__dict__)
    div()

def main(**kwargs):

    gl = init_gitlab()

    pid = kwargs.get('pid')
    newname = kwargs.get('newname')

    project = gl.projects.get(pid)
    print("PROJECT => {}".format(project.name))
    mb = project.branches.get('master')

    '''
        TODO: Add logic to check project rules associated with master. 
    '''
    project_rules = project.pushrules.get()
    #
    # # GET PROTECTED DATA ABOUT MASTER - WE MAY NEED TO GO USE THIS DATA FOR NEW BRANCH
    # pb = project.protectedbranches.get('master')
    # div("master protection")
    # pprint.pprint(pb.__dict__)
    # div()

    # CREATE NEW BRANCH
    try:
        nb = project.branches.create({'branch': newname, 'ref': 'master'})
    except gitlab.exceptions.GitlabCreateError as e:
        # CATCH IF BRANCH ALREADY EXISTS
        nb = project.branches.get(newname)


    # PROTECT NEW BRANCH
    nb.protect(developers_can_push=False, developers_can_merge=False)

    # SET NEW BRANCH AS DEFAULT
    project.default_branch = nb.name
    project.save()

    # DELETE OLD DEFAULT
    mb.delete()
    print("Your new default branch is {}".format(nb.name))
    print("DON'T FORGET TO UPDATE YOUR AUTOMATION")

if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    sys.exit(main(**vars(args)))
